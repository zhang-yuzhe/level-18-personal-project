import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class demo {
    public static void main(String[] args) {
        File file = new File("Gone_with_the_wind.txt");
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file),"GBK"));//构造一个BufferedReader类来读取文件
            String s = null;
            String l="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            //定义存储各字母出现次数的数组
            int []counter=new int[52];
            //定义存储各字母出现频率的数组
            double []f2=new double[52];
            //定义字母总个数的计数器。排除标点符号，空格和数字。
            int total_counter=0;
            char [] m =l.toCharArray();
            while((s = br.readLine())!=null){//使用readLine方法，一次读一行
                char [] c =s.toCharArray();

                for(int i=0;i<52;i++) {
                    //内层循环，遍历待计算文本
                    for (int j = 0; j < c.length; j++) {
                        //字母每在文本中出现一次，字母计数器加一,总字母个数计数器加一
                        if (c[j]==m[i]) {
                            counter[i]++;
                            total_counter++;
                        }
                    }
                }
            }
            for(int i=0;i<52;i++)
            {
                //将int类型转换成double类型
                double counter_d=(double) counter[i];
                double total_d=(double) total_counter;
                //字母出现频率=字母出现次数/总的字母数
                double fre=counter_d/total_d;
                //保留小数点后4位
                f2[i]=(double) (Math.round(fre*100)/100.0);
                //输出结果

            }
            Map<Character, Double> map = new TreeMap<>();
            for (int i = 0; i < m.length; i++) {
                map.put(m[i],f2[i]);
            }
            Map<Character, Double> result1 = map.entrySet().stream()
                    .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                            (oldValue, newValue) -> oldValue, LinkedHashMap::new));

            System.out.println(result1);


            br.close();
        }catch (Exception e){

        }
    }
}
