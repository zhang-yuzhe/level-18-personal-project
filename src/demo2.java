import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class demo2{
    public static void main(String[] args) {
        File file = new File("Gone_with_the_wind.txt");
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "GBK"));//构造一个BufferedReader类来读取文件
            Map<String, Integer> map =new TreeMap<>();
            String str = null;
            String s [] =null;
            while ((str= br.readLine()) != null) {//使用readLine方法，一次读一行
              s = str.split(" ");

                for (int i = 0; i < s.length; i++) {
                        String s0 = s[i].replaceAll("\\pP|\\pS|\\pC|\\pN|\\pZ", "").toLowerCase();
                        if(s0==null||"".equals(s0)){
                            continue;
                        }
                        if (true) {
                            if ("".equals(s0)) {
                                continue;
                            }
                            if (!map.containsKey(s0)) {
                                map.put(s0, 1);
                            } else {
                                map.put(s0, map.get(s0) + 1);
                            }
                        }

                }
            }

            Map<String, Integer> result = map.entrySet().stream()
                    .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                            (oldValue, newValue) -> oldValue, LinkedHashMap::new));
            Scanner sc = new Scanner(System.in);
            System.out.println("请输入要要查前几个单词：");
            Iterator<String> iterator = null;
            int n = sc.nextInt();
            String a[]=new String[n];
            int b[]=new int[n];

            for(int i=0;i<n;i++)
            {
                iterator = result.keySet().iterator();
                while(iterator.hasNext()){
                    String word = (String) iterator.next();
                    if(b[i]<result.get(word))
                    {
                        b[i]=result.get(word);
                        a[i]=word;
                    }
                }
                result.remove(a[i]);
            }
            for (int i=0;i<a.length;i++) {
                System.out.println(i+1+"."+a[i] + "出现了" + b[i] + "次");
            }
            br.close();
        } catch (Exception e) {

        }


    }
}
